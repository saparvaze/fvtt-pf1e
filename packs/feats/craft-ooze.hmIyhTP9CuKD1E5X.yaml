_id: hmIyhTP9CuKD1E5X
_key: '!items!hmIyhTP9CuKD1E5X'
img: icons/creatures/slimes/slime-bubble-green.webp
name: Craft Ooze
system:
  description:
    value: >-
      <p><em>You can use alchemy to create dangerous ooze
      creatures.</em></p><p><strong>Prerequisites</strong>:
      @UUID[Compendium.pf1.feats.Item.nNVFVFRBCT4O2ab9]{Brew Potion},
      @UUID[Compendium.pf1.feats.Item.tD76K8QGco9scT0r]{Craft Wondrous Item},
      Craft (alchemy) 3 ranks, caster level 5th.</p><p><strong>Benefit</strong>:
      You can create living oozes as though they were magical items. Creating an
      ooze creature takes 1 day for each 500 gp in its construction cost. To
      create an ooze, you must have access to an oozing vat (see below), you
      must use up raw materials worth the construction cost of the ooze, and you
      must succeed at a Craft (alchemy) check (DC 10 + 2 × the ooze’s CR). A
      failed check ruins the materials used, while a check that fails by 5 or
      more also results in an ooze that attacks its creator for 1d4 rounds
      before dissipating into useless waste material. A newly created ooze has
      average hit points for its Hit Dice. Oozes created with this feat are
      mindless and uncontrolled, and even normally intelligent oozes like
      slithering trackers that are created this way have no Intelligence
      score—nor any loyalty to their creator.</p><p>While ooze creatures cannot
      normally be purchased in traditional marketplaces, GMs who wish to include
      such an option in their games—perhaps with oozes sold as black market
      commodities— need only double the construction cost of a specific ooze
      creature in order to figure out a fair market price.</p><p>The following
      table lists some of the most commonly crafted oozes and their creation
      requirements. At the GM’s discretion, other types of ooze creatures can be
      created with this feat. Creatures from Pathfinder RPG Bestiary 2, 3, or 4
      are marked with a matching
      superscript.</p><table><tbody><tr><th><p>Ooze</p></th><th><p>Construction
      Cost</p></th><th><p>Craft DC</p></th></tr><tr><td><p>Gelatinous
      cube</p></td><td><p>1,600
      GP</p></td><td><p>16</p></td></tr><tr><td><p>Gray
      ooze</p></td><td><p>3,600
      GP</p></td><td><p>18</p></td></tr><tr><td><p>Slithering
      tracker<sup>B2</sup></p></td><td><p>3,600
      GP</p></td><td><p>18</p></td></tr><tr><td><p>Ochre
      jelly</p></td><td><p>4,900
      GP</p></td><td><p>20</p></td></tr><tr><td><p>Black
      pudding</p></td><td><p>8,100
      GP</p></td><td><p>24</p></td></tr><tr><td><p>Magma
      ooze<sup>B2</sup></p></td><td><p>8,100
      GP</p></td><td><p>24</p></td></tr><tr><td><p>Deathtrap
      ooze<sup>B3</sup></p></td><td><p>8,100
      GP</p></td><td><p>26</p></td></tr><tr><td><p>Carnivorous
      crystal<sup>B3</sup></p></td><td><p>16,900
      GP</p></td><td><p>32</p></td></tr></tbody></table>
  sources:
    - id: PZO9445
      pages: '22'
  tags:
    - General
    - Item Creation
type: feat
