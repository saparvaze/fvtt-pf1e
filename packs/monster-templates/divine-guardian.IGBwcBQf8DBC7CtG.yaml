_id: IGBwcBQf8DBC7CtG
_key: '!items!IGBwcBQf8DBC7CtG'
img: systems/pf1/icons/skills/violet_07.jpg
name: Divine Guardian
system:
  changes:
    - _id: yk1zk643
      formula: '4'
      target: wis
      type: untyped
    - _id: 8o4lv241
      formula: '4'
      target: cha
      type: untyped
    - _id: v9mn6iws
      formula: '5'
      target: skill.per
      type: racial
    - _id: 585jzhn9
      formula: '5'
      target: skill.sen
      type: racial
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>A divine guardian is a creature
      chosen by the gods to guard a sacred site of the faith. Blessed with
      eternal life (or damned, some might say), a divine guardian spends untold
      centuries in the service of its deity, tirelessly and deathlessly
      defending its charge from any who would seek to desecrate it.<p>Typically
      such a creature is transformed into a form more regal than its mortal one,
      setting it apart from a typical member of its race or species. A divine
      guardian is spiritually connected to the one site that it must guard for
      eternity. As long as a divine guardian remains within that site, it does
      not hunger, thirst, get sick, or even age. Within the bounds of its sacred
      site, a divine guardian possesses numerous defensive powers to ward it
      from intruders, but it can never leave the area or the long years of its
      service will finally catch up to it. A divine guardian must weigh the
      power and prestige of its endless responsibility against the freedom death
      might inevitably bring.<p>Most divine guardians are chosen servants who
      agree to willingly serve their gods for all eternity, but some have been
      cursed with their duty in response for some harm to the god’s faithful or
      as atonement for some great sin. Whatever the nature of its creation, a
      divine guardian is still beholden to the god that granted it its powers,
      and to the followers of that god as well.<p>A cleric or paladin of the
      deity that created a divine guardian can issue the guardian commands. This
      does not give the cleric or paladin complete control over the creature,
      but the guardian does respond favorably to those requests. For example, a
      cleric could ask it to not attack her companions, or to help her defend
      the guardian’s sacred site from attackers. A cleric or paladin of the same
      faith must win an opposed Charisma check to convince a divine guardian to
      do anything it wouldn’t ordinarily do. A divine guardian can never be
      ordered to leave its sacred site or to go against the tenets of its
      deity’s faith.<p>The divine guardian hydra presented here is built using a
      hydra from the Pathfinder RPG Bestiary. See page 178 of the Bestiary for
      rules on this creature’s hydra traits and regenerate head abilities. This
      divine guardian hydra is a divine guardian of the god of nature and the
      weather, thus giving it the air and water subtypes.<p>“Divine guardian” is
      an acquired template that can be added to any creature (referred to
      hereafter as the base creature). A divine guardian uses all the base
      creature’s statistics and special abilities except as noted
      here.<p><b>CR:</b> Same as the base creature +1.</p>

      <p><b>Alignment:</b> Usually, the alignment of a divine guardian matches
      that of the god who invested it with power. Sometimes, however, a god
      punishes a wayward worshiper or an enemy of the faith by making it a
      divine guardian.</p>

      <p><b>Type:</b> The creature’s type does not change, but the creature
      might gain one or more alignment or elemental subtypes, depending on the
      alignment and portfolio of the deity that granted it the template.
      Possible subtypes include air, chaotic, cold, earth, evil, fire, good,
      lawful, and water. For instance, a lawful good deity’s divine guardian
      would have the lawful and good subtypes, even if it were actually of some
      other alignment. Similarly, a neutral god of water and ice would grant its
      divine guardian the water and cold subtypes.</p>

      <p><b>Senses:</b> A divine guardian gains darkvision 60 feet and low-light
      vision.</p>

      <p><b>Defensive Abilities:</b> A divine guardian is immune to disease,
      poison, and all mind-affecting effects. It also gains fast healing 5. In
      addition, it gains the following defensive ability.</p>

      <p><em>Ability Healing (Ex): </em>A divine guardian heals 1 point of
      ability damage per round in each damaged ability score.</p>

      <p><b>Special Attacks:</b> A divine guardian gains the following.</p>

      <p><b>Spell-Like Abilities:</b> A divine guardian has a cumulative number
      of spell-like abilities depending on its Hit Dice. Unless otherwise noted,
      these abilities are usable 1/day. CL is equal to the divine guardian’s HD
      or the CL of the base creature’s spell-like abilities, whichever is
      higher.</p>

      <hr>

      <table>

      <tbody>

      <tr>

      <td><b>HD</b></td>

      <td><b>Abilities</b></td>

      </tr>

      <tr>

      <td>1–2</td>

      <td><em>Alarm</em> 3/day, <em>dimension door </em>at will (within sacred
      site only), <em>hold portal</em></td>

      </tr>

      <tr>

      <td>3–4</td>

      <td><em>Arcane lock</em>, <em>knock </em>3/day</td>

      </tr>

      <tr>

      <td>5–6</td>

      <td><em>Augury</em>, <em>clairaudience/clairvoyance</em></td>

      </tr>

      <tr>

      <td>7–8</td>

      <td><em>Dismissal</em></td>

      </tr>

      <tr>

      <td>9–10</td>

      <td><em>Commune</em></td>

      </tr>

      <tr>

      <td>11–12</td>

      <td><em>Guards and wards</em></td>

      </tr>

      <tr>

      <td>13–14</td>

      <td><em>Forbiddance</em></td>

      </tr>

      <tr>

      <td>15–16</td>

      <td><em>Banishment</em></td>

      </tr>

      <tr>

      <td>17–18</td>

      <td><em>Repulsion</em></td>

      </tr>

      <tr>

      <td>19–20</td>

      <td><em>Screen</em></td>

      </tr>

      <tr>

      <td>21+</td>

      <td><em>Antipathy</em></td>

      </tr>

      </tbody>

      </table>

      <hr>

      <p><br><b>Special Qualities:</b> A divine guardian gains the
      following.</p>

      <p><em>Aura (Ex): </em>A divine guardian with the chaotic, evil, good, or
      lawful subtypes has an aura as if it were an aligned outsider of
      equivalent Hit Dice (see the detect evil spell for details).</p>

      <p><em>Blessed Life (Ex): </em>A divine guardian does not age or breathe.
      It does not require food, drink, or sleep.</p>

      <p><em>Divine Swiftness (Ex): </em>A divine guardian is gifted with
      incredible speed, granting it a +4 bonus on initiative rolls. In addition,
      each of the base creature’s speeds is doubled. If the base creature has a
      fly speed, the divine guardian’s maneuverability becomes perfect if it was
      not already. If the divine guardian acquires the air, earth, or water
      subtype, it gains a fly, burrow, or swim speed equal to its highest
      speed.</p>

      <p><em>Sacred Site (Ex): </em>Each divine guardian is assigned to guard a
      specific site sacred to the deity that invested it with power. This area
      may be a structure, a series of structures, or a natural site with clearly
      defined borders. It can be as large as a city, but in most cases it’s a
      single temple or a sacred grove. Gods don’t waste their powers on places
      that their worshipers can protect, so most divine guardians keep watch
      over abandoned burial grounds or lost temples. The divine guardian of such
      a site is charged with protecting it from harm and preventing incursions
      by those not of the faith. It must keep its vigil until the god deems the
      guardian’s task done.<p>If the divine guardian ever moves out of the area
      defined as the sacred site, it immediately loses the divine guardian
      template and any spellcasting ability the deity might have granted from
      class levels. It cannot regain the template unless it atones for its
      failure (usually by completing some quest or via an atonement spell) and
      reenters the site within 1 week. Otherwise, it loses the template
      permanently, taking 6d6 points of Constitution drain as the years of lost
      food, drink, and sleep return to it tenfold. A creature that lacks a
      Constitution score takes 2d6 points of damage per Hit Die from this
      process. Even if it survives the Constitution drain, the creature can
      never regain the template.</p>

      <p><b>Abilities:</b> Wis +4, Cha +4. If the base creature has an
      Intelligence score of 2 or lower, it also gains Int +4.</p>

      <p><b>Skills:</b> A divine guardian gains a +5 racial bonus on Perception
      and Sense Motive checks.</p>

      <p><b>Organization:</b> Solitary.</p>
  subType: template
type: feat
