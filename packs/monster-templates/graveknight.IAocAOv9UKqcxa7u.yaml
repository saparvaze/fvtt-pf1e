_id: IAocAOv9UKqcxa7u
_key: '!items!IAocAOv9UKqcxa7u'
img: systems/pf1/icons/skills/violet_07.jpg
name: Graveknight
system:
  changes:
    - _id: zx03e1t1
      formula: '4'
      target: nac
      type: untyped
    - _id: p20wbork
      formula: '6'
      target: str
      type: untyped
    - _id: x8epjt5p
      formula: '2'
      target: int
      type: untyped
    - _id: qh9usy20
      formula: '4'
      target: wis
      type: untyped
    - _id: wjqrii3q
      formula: '4'
      target: cha
      type: untyped
    - _id: p59m8jqk
      formula: '8'
      target: skill.int
      type: racial
    - _id: sa4ljr2i
      formula: '8'
      target: skill.per
      type: racial
    - _id: n889iiyk
      formula: '8'
      target: skill.rid
      type: racial
  crOffset: '2'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>Undying tyrants and eternal
      champions of the undead, graveknights arise from the corpses of the most
      nefarious warlords and disgraced heroes—villains too merciless to submit
      to the shackles of death. They bear the same weapons and regalia they did
      in life, though warped or empowered by their profane resurrection. The
      legions they once held also flock to them in death, ready to serve their
      wicked ambitions once more. A graveknight’s essence is fundamentally tied
      to its armor, the bloodstained trappings of its battle lust. This armor
      becomes an icon of its perverse natures, transforming into a monstrous
      second skin over the husk of desiccated flesh and scarred bone locked
      within.<p>“Graveknight” is an acquired template that can be added to any
      living creature with 5 or more Hit Dice (referred to hereafter as the base
      creature). Most graveknights were once humanoids. A graveknight uses the
      base creature’s statistics and abilities except as noted
      here.<p><b>CR:</b> Same as base creature +2.</p>

      <p><b>Alignment:</b> Any evil.</p>

      <p><b>Type:</b> The graveknight’s type changes to undead (augmented). Do
      not recalculate class Hit Dice, BAB, or saves.</p>

      <p><b>Senses:</b> A graveknight gains darkvision 60 ft.</p>

      <p><b>Aura:</b> A graveknight emanates the following aura.</p>

      <p><em>Sacrilegious Aura (Su): </em>A graveknight constantly exudes an
      aura of intense evil and negative energy in a 30-foot radius. This aura
      functions as the spell desecrate and uses the graveknight’s armor as an
      altar of sorts to double the effects granted. The graveknight constantly
      gains the benefits of this effect (including the bonus hit points, as this
      aura is part of the graveknight’s creation). In addition, this miasma of
      fell energies hinders the channeling of positive energy. Any creature that
      attempts to summon positive energy in this area—such as through a cleric’s
      channel energy ability, a paladin’s lay on hands, or any spell with the
      healing subtype—must make a concentration check with a DC equal to 10 +
      1/2 the graveknight’s Hit Dice + the graveknight’s Charisma modifier. If
      the character fails, the effect is expended but does not function.</p>

      <p><b>Armor Class:</b> Natural armor improves by +4.</p>

      <p><b>Hit Dice:</b> Change all racial Hit Dice to d8s. Class Hit Dice are
      unaffected. As an undead, a graveknight uses its Charisma modifier to
      determine bonus hit points.</p>

      <p><b>Defensive Abilities:</b> A graveknight gains channel resistance +4;
      DR 10/magic; and immunity to cold, electricity, and any additional energy
      type noted by its ruinous revivification special quality. A graveknight
      also gains spell resistance equal to its augmented CR + 11.<br>The
      graveknight also gains the following ability.<p><em>Rejuvenation (Su):
      </em>One day after a graveknight is destroyed, its armor begins to rebuild
      the undead horror’s body. This process takes 1d10 days—if the body is
      destroyed before that time passes, the armor merely starts the process
      anew. After this time has elapsed, the graveknight wakens fully
      healed.<p><b>Attacks:</b> A graveknight gains a slam attack if the base
      creature didn’t have one. Damage for the slam depends on the graveknight’s
      size (see Bestiary 302).</p>

      <p><b>Special Attacks:</b> A graveknight gains the following special
      attacks. Save DCs are equal to 10 + 1/2 the graveknight’s HD + the
      graveknight’s Charisma modifier unless otherwise noted.<p><em>Channel
      Destruction (Su): </em>Any weapon a graveknight wields seethes with
      energy, and deals an additional 1d6 points of damage for every 4 Hit Dice
      the graveknight has. This additional damage is of the energy type
      determined by the ruinous revivification special quality.</p>

      <p><em>Devastating Blast (Su): </em>Three times per day, the graveknight
      may unleash a 30-foot cone of energy as a standard action. This blast
      deals 2d6 points of damage for every 3 Hit Dice a graveknight has (Reflex
      for half). This damage is of the energy type determined by the
      graveknight’s ruinous revivification special quality.</p>

      <p><em>Undead Mastery (Su): </em>As a standard action, a graveknight can
      attempt to bend any undead creature within 50 feet to its will. The
      targeted undead must succeed at a Will save or fall under the
      graveknight’s control. This control is permanent for unintelligent undead;
      an undead with an Intelligence score is allowed an additional save every
      day to break free from the graveknight’s control. A creature that
      successfully saves cannot be affected again by the same graveknight’s
      undead mastery for 24 hours.<p>A graveknight can control 5 Hit Dice of
      undead creatures for every Hit Die it has. If the graveknight exceeds this
      number, the excess from earlier uses of the ability becomes uncontrolled,
      as per animate dead.</p>

      <p><b>Special Qualities:</b> A graveknight gains the
      following.<p><em>Phantom Mount (Su): </em>Once per hour, a graveknight can
      summon a skeletal horse similar to a phantom steed. This mount is more
      real than a typical phantom steed, and can carry one additional rider. The
      mount’s powers are based on the graveknight’s total Hit Dice rather than
      caster level. A graveknight’s mount looks distinctive and always appears
      the same each time it is summoned. If the mount is destroyed, it can be
      summoned again with full hit points 1 hour later.</p>

      <p><em>Ruinous Revivification (Su): </em>At the time of its creation, the
      graveknight chooses one of the following energy types: acid, cold,
      electricity, or fire. This energy type should be relevant to the
      graveknight’s life or death, defaulting to fire if none are especially
      appropriate. This energy type influences the effects of several of a
      graveknight’s special abilities.<p><b>Ability Scores:</b> Str +6, Int +2,
      Wis +4, Cha +4. As an undead creature, a graveknight has no Constitution
      score.</p>

      <p><b>Skills:</b> Graveknights gain a +8 racial bonus on Intimidate,
      Perception, and Ride checks.</p>

      <p><b>Feats:</b> Graveknights gain Improved Initiative, Mounted Combat,
      Ride-By Attack, and Toughness as bonus feats.</p>
  subType: template
type: feat
