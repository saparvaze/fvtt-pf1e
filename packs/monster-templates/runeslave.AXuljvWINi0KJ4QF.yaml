_id: AXuljvWINi0KJ4QF
_key: '!items!AXuljvWINi0KJ4QF'
img: systems/pf1/icons/skills/violet_07.jpg
name: Runeslave
system:
  changes:
    - _id: rslb9djp
      formula: '20'
      target: landSpeed
      type: base
    - _id: 6oqlm8q0
      formula: '4'
      target: str
      type: untyped
    - _id: oeejn6sf
      formula: '2'
      target: dex
      type: untyped
    - _id: eylsh57o
      formula: '-2'
      target: int
      type: untyped
    - _id: tnn3f26k
      formula: '-2'
      target: wis
      type: untyped
    - _id: 3stmq4u0
      formula: '-2'
      target: cha
      type: untyped
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>The ageless monuments and awesome
      cities of Thassilon rose upon the backs of countless slaves, but none bore
      the sin-poisoned civilization’s burden more than the giants. Able to
      perform the work of dozens of human slaves, Thassilon’s titanic
      servants—hill giants, stone giants, taiga giants, and others—crafted
      marvels nigh unparalleled in any era before or since, and shaped the face
      of what is now modern Varisia. Yet as viciously as the runelords worked
      their slaves and for all they demanded, the giant-crafted marvels were not
      enough. And thus, working the corrupt rune magic that was theirs alone,
      the runelords manufactured a damning curse and laid it over their most
      tireless and effective workers, and in so doing created a new breed of
      servant: the runeslave.<p>Numerous severe-looking runes spark and flicker
      upon a runeslave’s body, seemingly seared into the creature’s flesh. One
      of the runes is larger and more prominent than the others—this is always
      one of the runes of Thassilonian magic. Although a runeslave’s mind is
      dulled, its muscles bulge grotesquely, as if barely contained beneath a
      thin layer of skin, and such behemoths move with unnatural agility for
      creatures of their ponderous size.<p>Note that while the runeslave
      template does make a giant more powerful (and thus increases its CR), few,
      if any giants would seek to gain a runeslave’s powers. Despite the
      advantages the runeslave gains, what it loses in free will and longevity
      typically vastly outweigh the benefits. In combat, a runeslave is deadly
      and terrifying, but in life, the condition is rightly feared among giants
      as a devastating and debilitating curse.<p>“Runeslave” is an acquired
      template that can be added to any giant (referred to hereafter as the base
      creature). A runeslave uses all the base creature’s statistics and special
      abilities except as noted here.<p><b>CR:</b> Same as the base creature
      +1.</p>

      <p><b>Defensive Abilities:</b> A runeslave becomes immune to fear effects,
      exhaustion, and fatigue. In addition, all runeslaves gain the following
      additional defensive ability.</p>

      <p><em>Resist Pain (Ex): </em>Runeslaves can continue to function even
      after taking great punishment. They are immune to nonlethal damage.
      Against effects that inflict pain (such as a symbol of pain spell), a
      runeslave gains a +4 bonus on all saving throws.<br>Weaknesses: Runeslaves
      gain the following weakness.</p>

      <p><em>Arcane Decay (Su): </em>The symbols etched upon a runeslave’s body
      put great stress on its physical form, choking its mind and ultimately
      killing the giant in time. Each runeslave has a predominant Thassilonian
      rune associated with one school of magic inscribed on its body.
      Traditionally, this rune is of a school of magic directly opposed to the
      runelord the runeslave serves— all of the runeslaves encountered in this
      adventure bear the sign of wrath upon their bodies as a sort of brand of
      shame. The slow decay of a runeslave’s mental faculties manifests as a
      gradual loss of life and sanity, represented by the accumulation of
      rune-shaped scars all over the body. The disease has no additional
      physical or mental effect until these magical runescars completely
      overwhelm their host, at which point the accumulated pain the giant has
      endured since becoming a runeslave is released in a fatal surge of
      unleashed suffering. All runeslaves are “infected” with this disease. Only
      limited wish, miracle, or wish can prevent or cure arcane decay, but in so
      doing removes the entire template, reverting the runeslave back to the
      base creature. Multiple successful Fortitude saves only delay the decay
      and do not cure the creature of the disease.</p>

      <p>Arcane Decay: Inherited—non-contagious; save Fortitude DC 15; frequency
      1/week; effect gain one runescar; cure none (but see above). When a
      runeslave’s number of runescars equals its Hit Dice, it dies.</p>

      <p><b>Speed:</b> A runeslave’s base land speed is 20 feet faster than the
      base creature’s. Other forms of movement, such as flying or swim speeds,
      are unaffected.</p>

      <p><b>Special Attacks:</b> A runeslave gains the following special
      attack.</p>

      <p><em>Arcane Surge (Su): </em>Once per day as a swift action, a runeslave
      can gain the benefits of the spell haste for 6 rounds. Using this ability
      forces the giant to make an additional Fortitude save against arcane
      decay, even if it has already made its weekly save to resist the
      disease.</p>

      <p><b>Abilities:</b> Change from the base creature as follows: Str +4, Dex
      +2, Int –2, Wis –2, Cha –2.</p>

      <p><b>Feats:</b> Runeslaves gain Diehard and Toughness as bonus feats.</p>
  subType: template
type: feat
