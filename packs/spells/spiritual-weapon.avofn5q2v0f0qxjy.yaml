_id: avofn5q2v0f0qxjy
_key: '!items!avofn5q2v0f0qxjy'
folder: NE0tEwSvcr71GnPC
img: icons/weapons/hammers/hammer-double-glowing-yellow.webp
name: Spiritual Weapon
system:
  actions:
    - _id: rpg166e9d8a2utgy
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      duration:
        dismiss: true
        units: round
        value: '@cl'
      effect: magic weapon of force
      name: Use
      range:
        units: medium
  components:
    divineFocus: 1
    somatic: true
    verbal: true
  description:
    value: >-
      <p>A weapon made of force appears and attacks foes at a distance, as you
      direct it, dealing 1d8 force damage per hit, + 1 point per three caster
      levels (maximum +5 at 15th level). The weapon takes the shape of a weapon
      favored by your deity or a weapon with some <i>spiritual</i> significance
      or symbolism to you (see below) and has the same threat range and critical
      multipliers as a real weapon of its form. It strikes the opponent you
      designate, starting with one attack in the round the spell is cast and
      continuing each round thereafter on your turn. It uses your base attack
      bonus (possibly allowing it multiple attacks per round in subsequent
      rounds) plus your Wisdom modifier as its attack bonus. It strikes as a
      spell, not as a weapon, so for example, it can damage creatures that have
      damage reduction. As a force effect, it can strike incorporeal creatures
      without the reduction in damage associated with incorporeality. The weapon
      always strikes from your direction. It does not get a flanking bonus or
      help a combatant get one. Your feats or combat actions do not affect the
      weapon. If the weapon goes beyond the spell range, if it goes out of your
      sight, or if you are not directing it, the weapon returns to you and
      hovers.</p><p>Each round after the first, you can use a move action to
      redirect the weapon to a new target. If you do not, the weapon continues
      to attack the previous round's target. On any round that the weapon
      switches targets, it gets one attack. Subsequent rounds of attacking that
      target allow the weapon to make multiple attacks if your base attack bonus
      would allow it to. Even if the <i>spiritual weapon</i> is a ranged weapon,
      use the spell's range, not the weapon's normal range increment, and
      switching targets still is a move action.</p><p>A <i>spiritual weapon</i>
      cannot be attacked or harmed by physical attacks, but <i>dispel magic</i>,
      <i>disintegrate</i>, a <i>sphere of annihilation</i>, or a <i>rod of
      cancellation</i> affects it. A <i>spiritual weapon</i>'s AC against touch
      attacks is 12 (10 + size bonus for Tiny object).</p><p>If an attacked
      creature has spell resistance, you make a caster level check (1d20 +
      caster level) against that spell resistance the first time the
      <i>spiritual weapon</i> strikes it. If the weapon is successfully
      resisted, the spell is dispelled. If not, the weapon has its normal full
      effect on that creature for the duration of the spell.</p><p>The weapon
      that you get is often a force replica of your deity's own personal weapon.
      A cleric without a deity gets a weapon based on his alignment. A neutral
      cleric without a deity can create a <i>spiritual weapon</i> of any
      alignment, provided he is acting at least generally in accord with that
      alignment at the time. The weapons associated with each alignment are as
      follows: chaos (battleaxe), evil (light flail), good (warhammer), law
      (longsword).</p>
  descriptors:
    value:
      - force
  learnedAt:
    class:
      cleric: 2
      inquisitor: 2
      medium: 2
      oracle: 2
      shaman: 2
      spiritualist: 2
      warpriest: 2
    domain:
      War: 2
  level: 2
  school: evo
  sources:
    - id: PZO1110
      pages: '348'
type: spell
