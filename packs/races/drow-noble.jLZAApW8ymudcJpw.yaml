_id: jLZAApW8ymudcJpw
_key: '!items!jLZAApW8ymudcJpw'
folder: CWHsfTdEmZohUkw1
img: icons/equipment/finger/ring-crown-silver-blue.webp
name: Drow Noble
system:
  changes:
    - _id: ux4kwop6
      formula: '4'
      target: dex
      type: racial
    - _id: gysjlugl
      formula: '2'
      target: cha
      type: racial
    - _id: pwxhb7mp
      formula: '-2'
      target: con
      type: racial
    - _id: x6at6cfs
      formula: '2'
      target: skill.per
      type: racial
    - _id: 76fnfqa1
      formula: '2'
      target: int
      type: racial
    - _id: ryqvevw7
      formula: '2'
      target: wis
      type: racial
  contextNotes:
    - target: allSavingThrows
      text: |-
        +[[2]] Racial vs Enchantment Effects
        Immune to Magic Sleep
  creatureSubtypes:
    custom: []
    value:
      - elf
  description:
    value: >-
      <h2>Starting
      Age</h2><table><thead><tr><th>Adulthood</th><th>Intuitive<sup>1</sup></th><th>Self-Taught<sup>2</sup></th><th>Trained<sup>3</sup></th></tr></thead><tbody><tr><td>110
      years</td><td><p>+4d6 years<br />(114 – 134 years)</p></td><td><p>+6d6
      years<br />(116 – 146 years)</p></td><td><p>+10d6 years<br />(120 – 170
      years)</p></td></tr></tbody></table><p><sup>1</sup> This category includes
      barbarians, oracles, rogues, and sorcerers.<br /><sup>2</sup> This
      category includes bards, cavaliers, fighters, gunslingers, paladins,
      rangers, summoners, and witches.<br /><sup>3</sup> This category includes
      alchemists, clerics, druids, inquisitors, magi, monks, and
      wizards.</p><h2>Height and
      Weight</h2><table><thead><tr><th>Gender</th><th>Base Height</th><th>Height
      Modifier</th><th>Base Weight</th><th>Weight
      Modifier</th></tr></thead><tbody><tr><td>Male</td><td>5 ft. 4
      in.</td><td><p>+2d6 in.<br />(5 ft. 6 in. – 6 ft. 4 in.)</p></td><td>90
      lbs.</td><td><p>+(2d6×5 lbs.)<br />(100 – 150
      lbs.)</p></td></tr><tr><td>Female</td><td>5 ft. 4 in.</td><td><p>+2d8
      in.<br />(5 ft. 6 in. – 6 ft. 8 in.)</p></td><td>110
      lbs.</td><td><p>+(2d8×5 lbs.)<br />(120 – 190
      lbs.)</p></td></tr></tbody></table><h2>Standard Racial
      Traits</h2><ul><li><strong>Ability Score Modifier</strong>: Drow are
      nimble and manipulative. They gain +4 <em>Dexterity</em>, +2 Intelligence,
      +2 Wisdom, +2 <em>Charisma</em>, and –2
      <em>Constitution</em>.</li><li><strong>Type</strong>: Drow are
      <em>humanoids</em> with the <em>elf</em>
      subtype.</li><li><strong>Size</strong>: Drow are Medium creatures and thus
      receive no bonuses or penalties due to their size.</li><li><strong>Base
      Speed</strong>: Drow have a base speed of 30
      feet.</li><li><strong>Languages</strong>: Drow begin play speaking Elven
      and Undercommon. Drow with high <em>Intelligence</em> scores can choose
      from the following languages: Abyssal, Aklo, Aquan, Common, Draconic, Drow
      Sign Language, Gnome, or Goblin.</li></ul><h3>Defense Racial
      Traits</h3><ul><li><strong>Immunities</strong>: Drow are immune to magic
      <em>sleep</em> effects and gain a +2 <em>racial bonus</em> on saving
      throws against <em>enchantment</em> spells and
      effects.</li><li><strong>Spell Resistance</strong>: Drow Nobles possess
      <em>spell resistance</em> (SR) equal to 11 plus their total number of
      class levels.</li></ul><h3>Feat and Skill Racial
      Traits</h3><ul><li><strong>Keen Senses</strong>: Drow gain a +2 <em>racial
      bonus</em> on
      @UUID[Compendium.pf1.pf1e-rules.x175kVqUfLGPt8tC.JournalEntryPage.2h6hz5AkTDxKPFxr]{Perception}
      checks.</li></ul><h3>Magical Racial Traits</h3><ul><li><strong>Spell-Like
      Abilities (Su)</strong>: Drow Nobles can cast
      @Compendium[pf1.spells.zymaptg3vmnvfvxl]{Dancing Lights},
      @Compendium[pf1.spells.jdsvncnna6oy189a]{Deeper Darkness},
      @Compendium[pf1.spells.bl71og1gklwncmt7]{Faerie Fire},
      @Compendium[pf1.spells.68m9du7zw2di7ew6]{Feather Fall} and
      @Compendium[pf1.spells.plou8h168bfn5hq6]{Levitate} each at will, and have
      @Compendium[pf1.spells.uqh87jz757r2cb7r]{Detect Magic} as a constant
      spell-like ability. A drow noble can also cast
      @Compendium[pf1.spells.21bxbzdaawjrnvyo]{divine favor},
      @Compendium[pf1.spells.gmgwyjfpeuuc4t4o]{dispel magic}, and
      @Compendium[pf1.spells.zqj5qzyl46af27v0]{suggestion} once per day each. In
      some cases, a drow noble's spell-like abilities might vary, although the
      level of a particular spell-like ability does not. A drow noble's caster
      level for her spell-like abilities is equal to her character
      level.</li></ul><h3>Offensive Racial Traits</h3><ul><li><strong>Poison
      Use</strong>: Drow are skilled in the use of <em>poisons</em> and never
      risk accidentally poisoning themselves.</li><li><strong>Weapon
      Familiarity</strong>: Drow are proficient with the
      @Compendium[pf1.weapons-and-ammo.BgTqaNqyS3tnLGAg]{Hand Crossbow},
      @Compendium[pf1.weapons-and-ammo.LA6TC5679iOXDNwq]{Rapier}, and
      @Compendium[pf1.weapons-and-ammo.05rD6YP5sVSrKY28]{Shortsword}.</li></ul><h3>Senses
      Racial Traits</h3><ul><li><strong>Superior Darkvision</strong>: Drow have
      superior darkvision, allowing them to see perfectly in the dark up to 120
      feet.</li></ul><h3>Weakness Racial Traits</h3><ul><li><strong>Light
      Blindness</strong>: As deep underground dwellers naturally, drow suffer
      from <em>light blindness</em>. Abrupt exposure to any bright light
      @UUID[Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.A9KUpd2bsdZZsQqj]{blinds}
      drow for 1 round. On subsequent rounds, they are
      @UUID[Compendium.pf1.pf1e-rules.NSqfXaj4MevUR2uJ.JournalEntryPage.xHUnCadQ2qYsfvV0]{dazzled}
      as long as they remain in the affected area.</li></ul>
  languages:
    value:
      - undercommon
      - elven
  sources:
    - id: PZO1112
      pages: '115'
  speeds:
    land: 30
  tags:
    - Bestiary
    - Darklands
  weaponProf:
    custom:
      - Hand Crossbow
      - Rapier
      - Shortsword
type: race
