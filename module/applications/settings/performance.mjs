import { AbstractSettingsApplication } from "@app/settings/abstract-settings.mjs";

export class PerformanceConfigModel extends foundry.abstract.DataModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      reachLimit: new fields.NumberField({ integer: true, min: 0, initial: 60 }),
    };
  }
}

/**
 * An application that lets the user configure performance-related settings.
 *
 * @augments {AbstractSettingsApplication}
 */
export class PerformanceConfig extends AbstractSettingsApplication {
  static DEFAULT_OPTIONS = {
    configKey: "performance",
    phraseKey: "PF1.Application.Settings.Performance",
    model: PerformanceConfigModel,
    window: {
      icon: "fa-solid fa-gauge",
    },
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/settings/performance.hbs",
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };
}
