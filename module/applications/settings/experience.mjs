import { AbstractSettingsApplication } from "@app/settings/abstract-settings.mjs";

export class ExperienceConfigModel extends foundry.abstract.DataModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    return {
      disable: new fields.BooleanField({ initial: false }),
      track: new fields.StringField({ initial: "medium", choices: ["fast", "medium", "slow", "custom"] }),
      custom: new fields.SchemaField({
        formula: new fields.StringField({ initial: "", nullable: false }),
      }),
      openDistributor: new fields.BooleanField({ initial: true }),
    };
  }

  static migrateData(source) {
    source.disable ??= source.disableExperienceTracking;
    if (source.track === "customFormula") source.track = "custom";
    source.openDistributor ??= source.openXpDistributor;

    return super.migrateData(source);
  }

  static get progressionOptions() {
    return {
      slow: "PF1.Application.Settings.Experience.Track.Options.Slow",
      medium: "PF1.Application.Settings.Experience.Track.Options.Medium",
      fast: "PF1.Application.Settings.Experience.Track.Options.Fast",
      custom: "PF1.Application.Settings.Experience.Track.Options.Custom",
    };
  }
}

/**
 * An application that lets the user configure experience related settings.
 *
 * @augments {AbstractSettingsApplication}
 */
export class ExperienceConfig extends AbstractSettingsApplication {
  static DEFAULT_OPTIONS = {
    configKey: "experienceConfig",
    phraseKey: "PF1.Application.Settings.Experience",
    model: ExperienceConfigModel,
    window: {
      icon: "fas fa-book",
    },
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/settings/experience.hbs",
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   * @param {string} partId
   * @param {ApplicationRenderContext} context
   * @returns {Promise<ApplicationRenderContext>}
   */
  async _preparePartContext(partId, context) {
    context = await super._preparePartContext(partId, context);
    if (partId !== "form") return context;

    Object.assign(context, {
      enabled: this.settings.disable !== true,
      hasCustomFormula: this.settings.track === "custom",
    });

    return context;
  }
}
