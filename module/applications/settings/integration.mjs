import { AbstractSettingsApplication } from "@app/settings/abstract-settings.mjs";

export class IntegrationConfigModel extends foundry.abstract.DataModel {
  static defineSchema() {
    const fields = foundry.data.fields;
    return {
      diceSoNice: new fields.BooleanField({ initial: true }),
      dragRuler: new fields.BooleanField({ initial: true }),
    };
  }
}

/**
 * An application that lets the user configure module integration related settings.
 *
 * @augments {AbstractSettingsApplication}
 */
export class IntegrationConfig extends AbstractSettingsApplication {
  static DEFAULT_OPTIONS = {
    configKey: "integration",
    phraseKey: "PF1.Application.Settings.Integration",
    model: IntegrationConfigModel,
    window: {
      icon: "fa-solid fa-check-to-slot",
    },
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/settings/integration.hbs",
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   * @param {string} partId
   * @param {ApplicationRenderContext} context
   * @returns {Promise<ApplicationRenderContext>}
   */
  async _preparePartContext(partId, context) {
    context = await super._preparePartContext(partId, context);
    if (partId !== "form") return context;

    Object.assign(context, {
      dsnFound: game.modules.get("dice-so-nice")?.active,
      drFound: game.modules.get("drag-ruler")?.active,
    });

    return context;
  }
}
