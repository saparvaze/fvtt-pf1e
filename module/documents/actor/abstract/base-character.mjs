import { ActorPF } from "@actor/actor-pf.mjs";

/**
 * Shared functionality for {@link pf1.documents.actor.ActorCharacterPF PCs} and {@link pf1.documents.actor.ActorNPCPF NPCs}.
 *
 * @todo Move PC/NPC functionality from ActorPF here.
 *
 * @abstract
 */
export class BaseCharacterPF extends ActorPF {
  /**
   * @protected
   * @override
   */
  prepareBaseData() {
    super.prepareBaseData();

    const system = this.system;

    system.traits ??= {};

    // Prepare size data
    const baseSize = system.traits.size || "med";
    const sizeValue = Object.keys(pf1.config.sizeChart).indexOf(baseSize);
    system.traits.size = {
      base: baseSize,
      value: sizeValue,
    };

    // Prepare age category data
    const baseAge = system.traits.ageCategory || "adult";
    const ageValue = Object.keys(pf1.config.ageCategories).indexOf(baseAge);
    system.traits.ageCategory = {
      base: baseAge,
      value: ageValue,
      physical: ageValue,
      mental: ageValue,
    };
  }
}
