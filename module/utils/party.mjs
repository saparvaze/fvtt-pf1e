const { HandlebarsApplicationMixin, ApplicationV2 } = foundry.applications.api;

export class PartyRestConfig extends HandlebarsApplicationMixin(ApplicationV2) {
  constructor(config, options) {
    super(options);

    config.hours ??= null;
    this.restConfig = config;

    this.constructor._prepareActors(config);

    this.constructor._calculateHours(config, config.hours === null);
  }

  static DEFAULT_OPTIONS = {
    tag: "form",
    form: {
      handler: this._onSubmit,
      submitOnChange: true,
      closeOnSubmit: false,
    },
    classes: ["pf1-v2", "party-rest"],
    window: {
      title: "PF1.Application.XPDistributor.Title",
      minimizable: false,
      resizable: true,
    },
    actions: {
      rest: this._onRest,
      toggle: this._onToggleCategory,
      /*
      cancel: ExperienceDistributor._cancel,
      split: ExperienceDistributor._split,
      give: ExperienceDistributor._giveAll,
      */
    },
    position: {
      width: 500,
    },
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/party-rest.hbs",
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  static _prepareActors(config) {
    const { longTermCare, restoreHealth, restoreUses, restOptions } = config;

    config.actors = config.actors
      .map((a) => {
        if (["haunt", "vehicle", "trap"].includes(a.type)) return null;

        const conscious = this._isConscious(a);
        return {
          name: a.token?.name || a.name,
          img: a.token?.img || a.prototypeToken?.texture?.src || a.img,
          uuid: a.uuid,
          conscious,
          watch: conscious,
          options: {
            ...restOptions,
            longTermCare,
            restoreHealth,
            restoreUses,
          },
        };
      })
      .filter((a) => !!a);
  }

  static _isConscious(actor) {
    const hpconf = game.settings.get("pf1", "healthConfig").variants;
    const variant = actor.type === "npc" ? hpconf.npc : hpconf.pc;
    const vigor = variant.useWoundsAndVigor;

    // TODO: Wounds & Vigor
    if (vigor) {
      const hp = actor.system.attributes.wounds;
      return hp.value >= hp.threshold;
    }
    // normal health
    else {
      const hp = actor.system.attributes.hp;
      return hp.value + hp.temp >= 0;
    }
  }

  static _calculateHours(config, updateTime = true) {
    const nowatch = config.watches === "none";
    const duo = config.watches === "duo";
    const solo = !duo;

    // Count watchers and pretend zero watchers results in math related to single person sleeping
    const active = nowatch ? 1 : Math.max(1, config.actors.filter((a) => a.watch).length);

    const cfg = pf1.config.partyRest[active] ?? Object.values(pf1.config.partyRest).at(-1);

    let hours = config.hours;
    // Skip time update if it was manually adjusted
    if (updateTime) {
      if (solo) hours = cfg.hours;
      else hours = cfg.double ?? cfg.hours;
      config.hours = hours;
    }

    // Set time needed per watch
    if (active > 1) {
      const watchTime = Math.clamp(hours, 0, 24);
      config.watchTime = watchTime / (solo ? active : Math.floor(active / 2));
    } else {
      config.watchTime = 0;
    }

    // Check if double watches are available for this group with this many watchers
    config.duo = cfg.double ?? false;
    if (!config.duo && duo) config.watches = "solo";
  }

  get title() {
    return game.i18n.localize("PF1.Application.PartyRest.Title");
  }

  /** @override */
  _prepareContext() {
    const config = this.restConfig;

    return {
      ...config,
      buttons: [
        {
          type: "button",
          label: "PF1.Rest",
          icon: "fa-solid fa-bed",
          action: "rest",
        },
      ],
    };
  }

  /**
   * Toggle rest category for all participants.
   *
   * @param {Event} event
   * @param {Element} el
   */
  static _onToggleCategory(event, el) {
    const config = this.restConfig;

    const toggle = el.dataset.toggle;
    const isWatch = toggle === "watch";
    const state = config.actors.some((a) => (isWatch ? !a.watch : !a.options[toggle]));
    config.actors.forEach((a) => {
      if (isWatch) a.watch = state;
      else a.options[toggle] = state;
    });

    if (isWatch) this.constructor._calculateHours(config);

    this.render();
  }

  /**
   * Rest button handler
   *
   * @param {Event} event
   * @param {Element} el
   */
  static async _onRest(event, el) {
    //event.preventDefault();

    event.pf1NoRender = true;

    // Ensure current state is submitted
    await this._onSubmitForm(this.options.form, event);

    const config = this.restConfig;

    // Disable inputs
    const content = this.element.querySelector(".window-content");
    content.classList.add("resting");
    for (const el of content.querySelectorAll("input,button")) {
      el.disabled = true;
    }

    const rv = await this.constructor._performRest(config);
    if (rv !== false) {
      ui.notifications.info(
        game.i18n.format("PF1.Application.PartyRest.Rested", {
          people: config.actors.length,
          hours: pf1.utils.limitPrecision(config.hours, 1),
        })
      );
    } else {
      config.cancelled = true;
    }
    this.resolve(config);
    this.close();
  }

  /**
   * Perform rest on each individual actor
   *
   * @param config
   * @internal
   */
  static async _performRest(config) {
    if (Hooks.call("pf1PrePartyRest", config) === false) return false;

    const { actors, hours } = config;

    const promises = [];
    for (const actorData of actors) {
      const { restoreHealth, restoreUses, longTermCare, ...options } = actorData.options;
      const actor = fromUuidSync(actorData.uuid);
      if (!actor) continue;
      const p = actor.performRest({ ...options, restoreHealth, restoreDailyUses: restoreUses, longTermCare });
      promises.push(p);
    }

    await Promise.allSettled(promises);

    if (hours > 0 && game.user.isGM) await game.time.advance(Math.floor(hours * 3_600));

    Hooks.callAll("pf1PartyRest", config);
  }

  /**
   * @param {Event} event
   * @param {Element} form
   * @param {FormDataExtended} formData
   */
  static _onSubmit(event, form, formData) {
    const config = this.restConfig;

    formData = foundry.utils.expandObject(formData.object);
    formData.actors = Object.values(formData.actors).map((a, idx) => foundry.utils.mergeObject(config.actors[idx], a));
    foundry.utils.mergeObject(config, formData);

    this.constructor._calculateHours(config, event.target.name !== "hours");

    if (event.pf1NoRender !== true) this.render();
  }

  /**
   * @override
   */
  close(...args) {
    super.close(...args);
    this.resolve(null);
  }

  /**
   * @override
   * @param {JQuery<HTMLElement>} jq
   */
  activateListeners(jq) {
    super.activateListeners(jq);

    const html = jq[0];

    // Header toggling all in column
    html.querySelectorAll(".toggle-all").forEach((el) => {
      el.addEventListener("click", (ev) => this._onToggle(ev));
    });

    // Allow opening character sheet
    html.querySelectorAll(".actor").forEach((el) => {
      el.addEventListener("contextmenu", (ev) => {
        ev.preventDefault();

        fromUuidSync(ev.target.dataset.actorUuid).sheet.render(true);
      });
    });
  }

  static async quickRest({
    actors = [],
    hours,
    restoreUses = true,
    restoreHealth = true,
    longTermCare = true,
    restOptions = {},
    watches = "none",
  } = {}) {
    const config = { actors, hours, restoreUses, restoreHealth, longTermCare, restOptions, watches };

    this._prepareActors(config);

    this._calculateHours(config, hours === undefined);

    await this._performRest(config);

    return config;
  }

  static async open(options) {
    return new Promise((resolve) => {
      const rd = new PartyRestConfig(options);
      rd.resolve = resolve;
      rd.render(true, { focus: true });
    });
  }
}

/**
 * Rest Party
 *
 * Time is passed if used by the GM.
 *
 * @param {options} options - Additional options
 * @param {Actor[]} options.actors - Actors to rest
 * @param {boolean} [options.watches=true] - People keep watch shifts.
 * @param {number} [options.hours=null] - How many hours to rest.
 * @param {boolean} [options.restoreHealth] - Restore health
 * @param {boolean} [options.restoreUses] - Restore daily uses
 * @param {boolean} [options.longTermCare] - Apply long term care
 * @param {object} [options.restOptions] - Additional options to pass to {@link ActorPF.performRest}
 * @returns {object|null} - Object with configuration resting was performed with, null if cancelled.
 */
export async function rest({
  actors = [],
  watches = true,
  hours = null,
  restoreHealth = true,
  restoreUses = true,
  longTermCare = false,
  skipDialog = false,
  restOptions = {},
} = {}) {
  actors = actors.filter((a) => a instanceof Actor && a.isOwner);
  if (actors.length == 0) throw new Error("No valid actors chosen to rest");

  if (typeof watches === "boolean") watches = watches ? "solo" : "none";

  if (skipDialog) {
    return PartyRestConfig.quickRest({ actors, hours, restoreUses, restoreHealth, longTermCare, restOptions });
  } else {
    return PartyRestConfig.open({ actors, watches, hours, restoreUses, restoreHealth, longTermCare, restOptions });
  }
}
